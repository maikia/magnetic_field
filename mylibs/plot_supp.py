import matplotlib.pylab as plt

import numpy as np

''' support for plotting functions '''
def clean_plot(ax):
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    #axis.set_axis_bgcolor('w')
    ax.set_frame_on(True)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()
    plt.xticks(fontsize=16)
    #plt.yticks(range(5000, 30001, 5000), fontsize=14)
    plt.yticks(fontsize=16)
    plt.xlabel('',fontsize=18)
    plt.ylabel('',fontsize=18)



def some_colors(number = 5, colormap=None):
    """ it returns list of given number of colors"""
    import colorsys
    N = number
    HSV_tuples = [(x*1.0/N, 1.0, 1.0) for x in range(N)]
    RGB_tuples = map(lambda x: colorsys.hsv_to_rgb(*x), HSV_tuples)

    # if only one color is required don't put in in the list
    if number == 1:
        RGB_tuples = RGB_tuples
    return RGB_tuples

def get_colors_for_segs(seg_coords, all_segs = ['soma', 'axon']):
    # it does work for the purpose but it should be improved to suit other purposes

    idx_colors = np.linspace(0,1, len(all_segs))
    colormap_seg = plt.cm.get_cmap('nipy_spectral')
    colors = []

    # this works for Hallermann model where axon[0] is ais
    for seg in seg_coords:
        if seg['name'] == 'axon[0]':
            seg_name = 'ais'
        else:
            seg_name = seg['name'].split('[')[0]
        idx_seg = all_segs.index(seg_name) 
        colors.append(colormap_seg(idx_colors[idx_seg]))  
    return colors, colormap_seg
